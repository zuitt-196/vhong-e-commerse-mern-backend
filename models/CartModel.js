const mongoose = require('mongoose');

const CartSchema =  new  mongoose.Schema({

        userId:{
            type: String,
            required: true
        },

        // So this Document has arry type which contain of what kind of project their hava get
        product:[
            {
                productId:{
                        type: String,

                },
                quantity:{
                    type: Number,
                    default:1
                }
            }
        ]
          
            

        


},
    {
        timestamps:true
    }
)

module.exports = mongoose.model('Cart', CartSchema);