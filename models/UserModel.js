const mongoose = require('mongoose');

const UserSchema =  new  mongoose.Schema({

        username:{
            type: String,
            required: [true, "Plase Enter your Name"],
            unique:true,
        },

        email:{
            type: String,
            required: [true, "Plase Enter your Email"],
            unique:true,

        },

        password:{
                type: String,
                required: [true, "Plase Enter your Password"], 
        },

         isAdmin:{
                    type: Boolean,
                    default:false,
                },
             

},
    {
        timestamps:true
    }
)

module.exports = mongoose.model('User', UserSchema);