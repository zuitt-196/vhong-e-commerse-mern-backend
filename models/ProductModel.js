const mongoose = require('mongoose');

const ProductSchema =  new  mongoose.Schema({

        title:{
            type: String,
            required: [true, "plase add Product"],
            unique:true,
        },

        desc:{
            type: String,
            required: [true, "plase add s Discription"],
            

        },

        image:{
                type: String,
                required: [true, "plase add Image"],

        },
        categories:{
            type: Array,
          
       },
        size:{
        type: String,
        

        },

        color:{
            type: String,

       },
       price:{
        type: Number, 
        required: [true, " Please add Price "],

   },


},
    {
        timestamps:true
    }
)

module.exports = mongoose.model('Product', ProductSchema);