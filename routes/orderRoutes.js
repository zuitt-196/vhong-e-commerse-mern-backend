const Order = require('../models/OrderModel');
const router =  require('express').Router();
const {verifyTokenAndAuthorization ,verifyTokenAndAdmin, verifyToken }= require("./verifyToken");




//CREATE HTTP METHOD ADD ORDER
router.post("/", verifyToken, async (req, res) => {
    const newOrder = await Order(
                req.body
        )
        try {
            const  saveProduct = await newOrder.save();
            res.status(200).json(saveProduct);
        } catch (error) {
            res.status(500).json({message: error.message})
        }
})


// UPDATE PRODUCT ORDER
router.put("/:id", verifyTokenAndAdmin , async (req, res) => {
    try {
        const updateOrder = await Order.findByIdAndUpdate(req.params.id, 
            {
            $set:req.body
            },{
                new:true
            }
            )

    res.status(200).json( updateOrder)
    } catch (error) {
        res.status(500).json({message: error.message})
    }
})






// DELETE HTTP METHOD ORDER
router.delete('/delete/:id', verifyTokenAndAdmin , async (req, res) => {
    // res.send("Dlete product")
    try {
        await Order.findByIdAndDelete(req.params.id)   
        res.status(200).json("Order has been deleted")
    } catch (error) {
        res.status(500).json({message: error.message})
    }
})


// GET USES ORDERS BY ID 
router.get('/findOrders/:userId',verifyTokenAndAuthorization, async (req, res) => {
        // res.send("finding single product")
        try {
            const product = await Order.find({userdId: req.params.userId});
            res.status(200).json(product);
          } catch (err) {
            res.status(500).json(err);
          }
})

// GET ALL USERS ORDERS
router.get("/getAllOrders", verifyTokenAndAdmin,async (req, res) => {
     try {
        const orders = await Order.find();
        res.status(200).json(orders);
     } catch (error) {
        res.status(500).json(err);
     }

})


// GET MONTLY INCOME 
router.get("/income", verifyTokenAndAdmin, async (req, res)=> {
    const date = new Date();
    // console.log("date:", date);
    const lastMonth = new Date(date.setMonth(date.getMonth() - 1));
    const previousMonth = new Date(new Date().setMonth(lastMonth.getMonth() - 1));

    // const lastMonth = new Date(date.setMontqh(date.getMonth() - 1));
    // const previousMonth = new Date(new Date().setMonth(lastMonth.getMonth() - 1));
    console.log("lastMonth:", lastMonth); 
    console.log("previousMonth:", previousMonth )

    try {
        const income = await Order.aggregate([
            //  comparing the both value used of gte having the value of bollean
            {$match: {createdAt:{$gte:previousMonth }} },

             {
                $project:{
                        month: {
                            $month:"$createdAt"
                        },
                       sales: "$amount" 
                },

            },
            {

                $group: {
                        _id: "$month",
                        total: {
                                $sum: "$sales"
                        },
                }

    }

]);
        console.log("income:", income)
        res.status(200).json(income)
        
    } catch (err) {
        res.status(500).json(err);
    }
     
})



module.exports = router