const jwt = require('jsonwebtoken')



const verifyToken   = (req, res , next) => {
        
     const authHeader = req.headers.token 
     if (authHeader) {
        const token = authHeader.split(' ')[1];
        jwt.verify(token, process.env.JWT_SECRET,(err,data)=>{
            if (err) {
                res.status(403).json("Token is not valid"); 
            }
            // THR SUCCESS TOKEN 
            req.data = data;
            console.log("user:" ,req.user)
            next();
        });
            
     }else{
            return res.status(401).json("You are not authenticated")
     }

}


// VERIFECATION AND AUTHORIZATIO
const verifyTokenAndAuthorization = function(req, res, next) {
    verifyToken(req, res, ()=>{
           
        if (req.data.id || req.data.isAdmin) {
            next()
        }else{
                res.status(403).json("you are not allowed to do that!")
        }


    })
}


// ADMIN ONLY THAT CAN ADD THE PRODUCT
const verifyTokenAndAdmin = (req, res, next) => {
    verifyToken(req, res, ()=>{
    if (req.data.isAdmin) {
        next()
    }else{
            res.status(403).json("you are not allowed to do that you are not admin!")
    }


})

}

module.exports =  {
    verifyToken ,
    verifyTokenAndAuthorization,
    verifyTokenAndAdmin


} ;